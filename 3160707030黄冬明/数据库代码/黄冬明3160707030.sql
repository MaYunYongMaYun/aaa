IF DB_ID('EduBase2018')IS NOT NULL
	DROP DATABASE EduBase2018;
CREATE DATABASE EduBase2018
 ON 
	(NAME='DataFile_1'
	,FILENAME='C:\EduBase2018\DataFile_1.mdf')
 LOG ON 
	(NAME='LogFile_1'
	,FILENAME='C:\EduBase2018\LogFile_1.ldf');
DROP DATABASE EduBase2018;

USE EduBase2018;
CREATE TABLE tb_Campus
(No
	CHAR(10)
	NOT NULL
,Name
	VARCHAR(10)
	NOT NULL)

CREATE TABLE tb_Department
(No
	CHAR(10)
	NOT NULL
,Name
	VARCHAR(40)
	NOT NULL)
	
CREATE TABLE tb_Degree
(No
	CHAR(10)
	NOT NULL
,Name
	VARCHAR(10)
	NOT NULL)

CREATE TABLE tb_Major
(No
	CHAR(10)
	NOT NULL
,Name
	VARCHAR(100)
	NOT NULL
,Abbreviation
	VARCHAR(10))
CREATE TABLE tb_Class
(No
	CHAR(10)
	NOT NULL
,Grade
	CHAR(4)
	NOT NULL
,Major
	VARCHAR(20)
	NOT NULL
,Class
	VARCHAR(20)
	NOT NULL
,Graduate
	BIT
	NOT NULL
,Campus
	VARCHAR(20)
	NOT NULL
)
CREATE TABLE tb_TeacherTitle
(
No 
	CHAR(10)
	NOT NULL
,Name
	VARCHAR(10)
	NOT NULL
,Rank
	VARCHAR(10)
	NOT NULL)
CREATE TABLE tb_Teacher
(
JobNo
	CHAR(7)
	NOT NULL
,Name
	VARCHAR(20)
	NOT NULL
,Gender
	BIT
	NOT NULL
,BirthDate
	DATE
	NOT NULL
,InaugurationDate
	DATE
	NOT NULL
,Telephone
	VARCHAR(20)
	NOT NULL
,TeacherTitle
	VARCHAR(20)
	NOT NULL
,Degree
	VARCHAR(20)
	NOT NULL)
CREATE TABLE tb_ClassType
(
No
	CHAR(7)
	NOT NULL
,Name 
	VARCHAR(20)
	NOT NULL)
CREATE TABLE tb_TestType
(
No
	VARCHAR(10)
	NOT NULL
Name
	VARCHAR(20)
	NOT NULL)
CREATE TABLE tb_Course
(
No
	CHAR(4)
	NOT NULL
,Name
	VARCHAR(100)
,Abbreviation
	VARCHAR(50)
	NOT NULL
,PrerequisiteCourse
	VARCHAR(50)
	NOT NULL
,Credit
	CHAR(3)
	NOT NULL
,ClassType
	VARCHAR(50)
	NOT NULL
,TestType
	VARCHAR(50)
	NOT NULL
	)
CREATE TABLE tb_CourseDetail
(
No
	CHAR(4)
	NOT NULL
,Intro
	VARCHAR(50)
	NOT NULL
,Abstract
	VARCHAR(50)
	NOT NULL
,Outline
	VARCHAR(50)
	NOT NULL
,Document
	VARCHAR(50)
	NOT NULL
,DocumentType
	VARCHAR(50)
	NOT NULL)
CREATE TABLE tb_Term
(
No
	CHAR(4)
	NOT NULL
,Mame
	VARCHAR(50)
	NOT NULL
	)
CREATE TABLE tb_Textbook
(
No
	CHAR(4)
	NOT NULL
,BookName
	VARCHAR(50)
	NOT NULL
,Press
	VARCHAR(100)
	NOT NULL
,Author
	VARCHAR(100)
	NOT NULL
,Price
	CURRENCY
	NOT NULL
,BookNo
	VARCHAR(20)
	NOT NULL)
CREATE TABLE tb_TeachTask
(
No
	VARCHAR(50)
	NOT NULL)
CREATE TABLE tb_Student
(
No
	CHAR(10)
	NOT NULL
,Name
	VARCHAR(20)
	NOT NULL
,Gender
	BIT
	NOT NULL
,Class
	VARCHAR(20)
	NOT NULL
,BirthDate
	DATE
	NOT NULL
,Telephone
	VARCHAR(20)
	NOT NULL
,Intro
	VARCHAR(20)
	NOT NULL
,Picture
	BLOB
	NOT NULL)


		