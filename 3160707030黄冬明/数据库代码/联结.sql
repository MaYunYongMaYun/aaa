SELECT 
		D.Name
		,M.Name 
		,M.Length
		,De.Name 
	FROM
		tb_Department as D 
		JOIN tb_Major as M ON D.No = M.DepartmentNo 
		JOIN tb_Degree as De ON M.DepartmentNo = De.No
SELECT
		D.Name 
		,RIGHT(C.Year,2)+ M.ShortName + isnull(C.AdministrationClass,'')
		
	FROM
		tb_Department as D 
		JOIN tb_Major as M ON D.No = M.DepartmentNo 
		JOIN tb_Class as C ON M.DepartmentNo = C.MajorNo
SELECT
		S.NAME + RIGHT(C.Year,2)+ M.ShortName + isnull(C.AdministrationClass,'')
	FROM
		tb_Major as M 
		JOIN tb_Class as C ON M.No = C.MajorNo
		join tb_Student AS S ON C.MajorNo = S.No
SELECT
		C.Name  
		,S.Name 
		,E.Name 
	FROM
		tb_Course AS C
		JOIN tb_StudyType AS S ON C.StudyTypeNo = S.No 
		JOIN tb_ExamType AS E ON S.No = E.No 
SELECT
		TT.TermNo 
		,T.Name 
		,F.Name 
		,FT.Name 
		,C.Name 
		,ST.Name 
		,B.Name 
	FROM
		tb_TeachingTask AS TT
		JOIN tb_Term AS T ON TT.TermNo = T.No 
		JOIN tb_Faculty AS F ON T.No = F.No
		JOIN tb_FacultyTitle AS FT ON F.No = Ft.Name 
		JOIN tb_Course AS C ON FT.No = C.DefaultFacultyNo 
		JOIN tb_StudyType AS ST ON C.DefaultFacultyNo = ST.No 
		JOIN tb_Book AS B ON ST.No = B.No 
SELECT	
		B.No AS StudentNo
		,B.Name 

 AS Name
		,RIGHT(C.Year,2)+D.ShortName+ISNULL(C.AdministrationClass,'') AS Class
		,F.Name 

 AS Term
		,G.Name 

 AS Faculty
		,H.Name 

 AS Course
		,A.FinalScore AS FinalScore
	FROM
		tb_StudentScore AS A
		JOIN tb_Student AS B ON A.StudentNo=B.No 
		JOIN tb_Class AS C ON B.ClassNo=C.No 
		JOIN tb_Major AS D ON C.MajorNo=D.No
		JOIN tb_TeachingTask AS E ON E.No=A.TeachingTaskNo  
		JOIN tb_Term AS F ON E.TermNo=F.No 
		JOIN tb_Faculty AS G ON  G.No=E.FacultyNo 
		JOIN tb_Course AS H ON H.No =E.CourseNo 
	
SELECT
		RIGHT(B.Year,2)+A.ShortName+ISNULL(B.AdministrationClass,'') AS Class
		,COUNT(1) AS PeopleNumber
	FROM 
		tb_Major  AS A
		JOIN tb_Class  AS B ON B.MajorNo=A.No 
		JOIN tb_Student AS C ON C.ClassNo=B.No 
	WHERE
		A.ShortName='信管'	
	GROUP BY
		RIGHT(B.Year,2)+A.ShortName+ISNULL(B.AdministrationClass,'')
	ORDER BY
		COUNT(1) DESC
SELECT
		SUM(F.Price) AS SUMPrice
	FROM   
		tb_StudentScore AS A
		JOIN tb_Student AS B ON B.No=A.StudentNo 
		JOIN tb_Class AS C ON C.No=B.ClassNo 
		JOIN tb_Major AS D ON D.No=C.MajorNo 
		JOIN tb_TeachingTask AS E ON E.No=A.TeachingTaskNo 
		JOIN tb_Book AS F ON F.No=E.BookNo 
	WHERE
		RIGHT(C.Year,2)+D.ShortName+ISNULL(C.AdministrationClass,'')='14信管'
		AND A.BookOrderFlag ='1'
SELECT
		A.Name 

 AS Major
		,RIGHT(B.Year,2)+A.ShortName+ISNULL(B.AdministrationClass,'') AS Class
	FROM 
		tb_Major AS A
		JOIN tb_Class AS B ON B.MajorNo=A.No 
	GROUP BY 
		ALL A.Name 

		,RIGHT(B.Year,2)+A.ShortName+ISNULL(B.AdministrationClass,'')
SELECT
		A.Name 

 AS 专业
		,COUNT(B.No)AS Number of Classes
	FROM
		tb_Major AS A
		JOIN tb_Class AS B ON B.MajorNo=A.No 
	GROUP BY
		ALL A.Name 	


		
