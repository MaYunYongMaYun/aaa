﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace 住院药房管理
{
    public partial class 常用药品目录查询 : Form
    {
        public 常用药品目录查询()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString =
                "Server=(local);Database=Hospha;Integrated Security=sspi";
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "SELECT * FROM tb_Changyong;";
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            sqlDataAdapter.SelectCommand = sqlCommand;
            DataTable studentTable = new DataTable();
            sqlConnection.Open();
            sqlDataAdapter.Fill(studentTable);
            sqlConnection.Close();
            this.dataGridView1.DataSource = studentTable; 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString =
                "Server=(local);Database=Hospha;Integrated Security=sspi";
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "SELECT * FROM tb_Changyong;";
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            sqlDataAdapter.SelectCommand = sqlCommand;
            sqlDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            DataTable CourseTable = new DataTable();
            sqlConnection.Open();
            sqlDataAdapter.Fill(CourseTable);

            sqlConnection.Close();
            DataRow searchResultRow = CourseTable.Rows.Find(this.textBox1.Text.Trim());
            DataTable searchResultTable = CourseTable.Clone();
            searchResultTable.ImportRow(searchResultRow);
            this.dataGridView1.DataSource = searchResultTable;
        }
    }
}
